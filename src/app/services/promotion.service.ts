import { Injectable } from '@angular/core';
import {Promotion} from '../shared/promotion';
import {PROMOTIONS} from '../shared/promotions';
import {of} from 'rxjs';
import {delay,map,filter} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {baseURL} from '../shared/baseurl';
@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: HttpClient) { }

  getPromotions(id: string):Observable<Promotion>{
    return this.http.get<Promotion>(baseURL + 'promotions/' + id);

  }
 getFeaturePromotion():Observable<Promotion>{
   return this.http.get<Promotion>(baseURL + 'promotions?featured=true').
   pipe(map((promo)=> promo[0]));
 }
}
