import { Component, OnInit, Inject,ViewChild} from '@angular/core';
import {Params,ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Dish} from '../shared/dish';
import {Comment} from '../shared/comment';
import {DishService} from '../services/dish.service';
import {switchMap} from 'rxjs/operators';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {visibility,flyInOut, expand} from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
  '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations:[ visibility(),
    flyInOut(),
    expand()]
})

export class DishdetailComponent implements OnInit {

  @ViewChild('cform') userCommentsDirecitve;

  dish: Dish;
  dishIds: string[];
  prev: string;
  next: string;
  date: Date;
  userComments: FormGroup;
  comments: Comment;
  errMess: string;
  dishCopy: Dish;
  visibility = 'shown';

  formErrors={
    'comment': '',
    'author':''
  };

  validationMessages ={
    'comment':{
      'required':'Comment is required',
      'maxLength':'comment should not exceed 255 characters'
    },
    'author':{
      'required':'Name field is required',
      'maxLength':'Name cannot exceed 25 characters',
      'minlength':'Name must be at least 2 characters'
    }
  }

  constructor( private dishService:DishService,
               private location:Location,
               private route:ActivatedRoute,
               private fb:FormBuilder,
               @Inject('BaseURL') private BaseURL) {

               this.createComment();
             }

  ngOnInit() {



    this.dishService.getDishIds()
     .subscribe(dishIds=>this.dishIds = dishIds,
      errmess=> this.errMess=errmess);

    this.route.params.pipe(switchMap( (params:Params)=>{this.visibility='hidden';
    return this.dishService.getDish(params['id']);}))
      .subscribe(dish=>{
        this.dish = dish;
        this.dishCopy = dish;
        this.setPrevNext(dish.id); this.visibility ='shown'},
        errmess=> this.errMess = errmess);
       }

  goBack():void{
    this.location.back();
  }

  setPrevNext(dishId:string){
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index -1)% this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index +1)% this.dishIds.length];
  }

 createComment(){
     this.userComments= this.fb.group({
       rating: [''],
       comment: ['',[Validators.required, Validators.maxLength(255)]],
       author: ['',[Validators.required, Validators.minLength(2)]],
       date: new Date().toISOString()
     });
       this.userComments.valueChanges.subscribe(data=>this.onValueChanged(data));
       this.onValueChanged();
 }

 onSubmit(){
   this.comments= this.userComments.value;
   this.dishCopy.comments.push( this.comments);
   this.dishService.putDish(this.dishCopy).subscribe(dish=>{
     this.dish=dish;
     this.dishCopy = dish;
   },errmess =>{this.dish = null; this.dishCopy = null; this.errMess = <any>errmess;});
   console.log(this.comments);
   this.userComments.reset({
     rating:"",
     comment:"",
     author: "",
     date: new Date().toISOString()
   });

   this.userCommentsDirecitve.resetForm();
 }

onValueChanged(data?: any){
  if(!this.userComments){return;}

  const form = this.userComments;
  for(const field in this.formErrors){
    if(this.formErrors.hasOwnProperty(field)){
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid){
        const messages = this.validationMessages[field];
        for (const key in control.errors){
          this.formErrors[field] += messages[key];
        }
      }
    }
  }
}
}
